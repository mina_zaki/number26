package com.number26.task.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akexorcist.googledirection.model.Step;
import com.number26.task.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mina on 15/02/16.
 */
public class DirectionListAdapter extends BaseAdapter {

    private List<Step> steps;
    private LayoutInflater inflater;

    public DirectionListAdapter(Context context){
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        steps = new ArrayList<>();
    }

    public void addItems(List<Step> items){
        steps = items;
    }

    public void clearItems(){
        steps.clear();
    }
    @Override
    public int getCount() {
        return steps.size();
    }

    @Override
    public Step getItem(int position) {
        return steps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Step step = getItem(position);
        View view = inflater.inflate(R.layout.direction_list_item, parent, false);
        ((TextView)view.findViewById(R.id.direction_text_view)).setText(step.getHtmlInstruction().replaceAll("\\<.*?>",""));

        return view;
    }
}
