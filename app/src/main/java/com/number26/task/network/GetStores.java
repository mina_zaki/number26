package com.number26.task.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLngBounds;
import com.number26.task.models.Store;
import com.number26.task.network.parsers.StoreJsonParser;
import com.number26.task.service_objects.MapServiceObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Mina on 10/02/16.
 */
public class GetStores extends BroadcastReceiver {

    private static final String urlString = "https://www.barzahlen.de/filialfinder/get_stores?map_bounds=";
    private static final String method = "GET";
    private Exception exception;
    private MapServiceObject callback;
    private GetStoresAsyncTask task;
    private boolean isCancelled;

    public GetStores(MapServiceObject callback) {
        this.callback = callback;
        task = new GetStoresAsyncTask();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(MapServiceObject.CANCEL_TASK_INTENT) && !isCancelled) {
            task.cancel(true);
            isCancelled = true;
        }
    }

    public void executeTask(LatLngBounds bounds) {
        task.execute(bounds);
    }

    private String getRequestStringFromBounds(LatLngBounds bounds) {
        String southWestBound = bounds.southwest.latitude + "," + bounds.southwest.longitude;
        String northEastBound = bounds.northeast.latitude + "," + bounds.northeast.longitude;
        String boundsString = "((" + southWestBound + "),(" + northEastBound + "))";
        return boundsString;
    }

    private InputStream fireRequest(LatLngBounds bounds) throws IOException {
        URL url = new URL(urlString + getRequestStringFromBounds(bounds));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.connect();
        return conn.getInputStream();
    }

    private class GetStoresAsyncTask extends AsyncTask<LatLngBounds, Integer, ArrayList<Store>> {

        @Override
        protected ArrayList<Store> doInBackground(LatLngBounds... params) {
            ArrayList<Store> stores = new ArrayList<>();
            InputStream is = null;
            try {
                if (!isCancelled()) {
                    is = fireRequest(params[0]);
                }
                if (!isCancelled()) {
                    stores = StoreJsonParser.parseStores(is);
                }
            } catch (IOException e) {
                exception = e;
            }
            return stores;
        }

        @Override
        protected void onPostExecute(ArrayList<Store> stores) {
            if (exception == null) {
                if (!isCancelled()) {
                    callback.getStoresRequestCallback(stores);
                }
            } else {
                callback.getStoresRequestCallbackError(exception);
            }
        }
    }
}

