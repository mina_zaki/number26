package com.number26.task.network.parsers;

import android.util.JsonReader;
import android.util.JsonToken;

import com.number26.task.models.OpeningHour;
import com.number26.task.models.Store;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Mina on 12/02/16.
 */
public class StoreJsonParser {

    public static ArrayList<Store> parseStores(InputStream is) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(is, "UTF-8"));
        ArrayList<Store> stores = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            stores.add(createStoreFromJson(reader));
            reader.endObject();
        }
        reader.endArray();
        reader.close();
        return stores;
    }

    private static Store createStoreFromJson(JsonReader reader) throws IOException {
        Store store = new Store();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (reader.peek() != JsonToken.NULL) {
                if (name.equals("id")) {
                    store.setId(reader.nextString());
                } else if (name.equals("title")) {
                    store.setTitle(reader.nextString());
                } else if (name.equals("city")) {
                    store.setCity(reader.nextString());
                } else if (name.equals("street_no") || name.equals("street_nr")) {
                    store.setStreet(reader.nextString());
                } else if (name.equals("opening_hours")) {
                    reader.beginArray();
                    store.setOpeningHours(createOpeningHoursFromJson(reader));
                    reader.endArray();
                } else if (name.equals("lat")) {
                    store.setLatitude(reader.nextDouble());
                } else if (name.equals("lng")) {
                    store.setLongitude(reader.nextDouble());
                } else {
                    reader.skipValue();
                }
            }
        }
        return store;
    }

    private static ArrayList<OpeningHour> createOpeningHoursFromJson(JsonReader reader) throws IOException {
        ArrayList<OpeningHour> openingHours = new ArrayList<>();
        while (reader.hasNext()) {
            OpeningHour openingHour = new OpeningHour();
            reader.beginObject();
            while (reader.hasNext()) {
                if (reader.peek() != JsonToken.NULL) {
                    String name = reader.nextName();
                    if (name.equals("days")) {
                        openingHour.setDays(reader.nextString());
                    } else if (name.equals("time")) {
                        openingHour.setTime(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }
            }
            openingHours.add(openingHour);
            reader.endObject();
        }
        return openingHours;
    }
}