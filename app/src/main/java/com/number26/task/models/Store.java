package com.number26.task.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mina on 10/02/16.
 */
public class Store {

    private String id;
    private String title;
    private String street;
    private String city;
    private double latitude;
    private double longitude;
    private String logoUrl;
    private List<OpeningHour> openingHours;

    public Store() {
        openingHours = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
    }

    public List<OpeningHour> getOpeningHours() {
        return openingHours;
    }
}
