package com.number26.task.models;

/**
 * Created by Mina on 10/02/16.
 */
public class OpeningHour {

    private String days;
    private String time;

    public OpeningHour() {
    }

    public void setDays(String days) {
        this.days = days;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDays() {
        return days;
    }

    public String getTime() {
        return time;
    }
}
