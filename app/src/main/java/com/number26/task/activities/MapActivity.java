package com.number26.task.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.number26.task.R;
import com.number26.task.adapters.DirectionListAdapter;
import com.number26.task.models.Store;
import com.number26.task.service_objects.MapServiceObject;

import org.json.JSONException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnCameraChangeListener, GoogleMap.OnMarkerClickListener, LocationListener, DirectionCallback {

    private MapServiceObject serviceObject;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private static final float DEFAULT_CAMERA_ZOOM = 15.0f;
    private static final float CAMERA_ZOOM_MAX = 18.0f;
    private static final float CAMERA_ZOOM_MIN = 13.0f;
    private Location currentLocation;
    private Polyline visiblePolyline;
    private ProgressBar progressBar;
    private Toast zoomLevelToast;
    private ListView directionsListView;
    private DirectionListAdapter directionsListAdapter;
    private ProgressBar directionsProgressBar;

    /*
        Activity related methods
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        setSupportActionBar((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar));
        mapFragment.getMapAsync(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_view);
        progressBar.setVisibility(View.GONE);
        serviceObject = MapServiceObject.getInstance();
        zoomLevelToast = Toast.makeText(this, "Invalid zoom level", Toast.LENGTH_SHORT);
        directionsListView = (ListView)findViewById(R.id.directions_list_view);
        directionsListAdapter = new DirectionListAdapter(this);
        directionsListView.setAdapter(directionsListAdapter);
        directionsProgressBar = (ProgressBar)findViewById(R.id.spinner);

        buildAPIClient();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    /*
        Map initialization and listeners
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (cameraPosition.zoom >= CAMERA_ZOOM_MIN && cameraPosition.zoom <= CAMERA_ZOOM_MAX) {
            LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
            serviceObject.fetchStores(bounds, this);
            zoomLevelToast.cancel();
            progressBar.setVisibility(View.VISIBLE);
        } else {
            zoomLevelToast.show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (currentLocation == null) {
            Toast.makeText(this, "Location unavailable, cant get directions", Toast.LENGTH_LONG).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            directionsProgressBar.setVisibility(View.VISIBLE);
            directionsListAdapter.clearItems();
            directionsListAdapter.notifyDataSetChanged();
            findViewById(R.id.directions_linear_layout).setVisibility(View.VISIBLE);
            String serverKey = getResources().getString(R.string.server_api_key);
            LatLng origin = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            GoogleDirection.withServerKey(serverKey).from(origin).to(marker.getPosition()).transportMode(TransportMode.WALKING).execute(this);
        }
        return false;
    }

    /*
        Google Api client
     */

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
        updateCurrentLocation();
        setCameraOnCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showAlertDialog("Google Api", "Google api is not working");
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        setCameraOnCurrentLocation();
    }

    /*
        Direction library
     */
    @Override
    public void onDirectionSuccess(Direction direction) {
        progressBar.setVisibility(View.GONE);
        directionsProgressBar.setVisibility(View.GONE);
        if (direction.getStatus().equals(RequestResult.OK)) {
            drawDirectionRoute(direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint());
            directionsListAdapter.addItems(direction.getRouteList().get(0).getLegList().get(0).getStepList());
            directionsListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        progressBar.setVisibility(View.GONE);
        showAlertDialog("Error", "We are sorry we cant get the directions for you now");
    }

    /*
        Service objects callbacks
     */

    public void fetchStoresCallback(ArrayList<Store> stores) {
        progressBar.setVisibility(View.GONE);
        for (Store store : stores) {
            MarkerOptions options = new MarkerOptions().position(new LatLng(store.getLatitude(), store.getLongitude()));
            options.snippet(store.getStreet());
            options.title(store.getTitle());
            mMap.addMarker(options);
        }
    }

    public void fetchStoresCallbackError(Exception e) {
        String title = null;
        String message = null;
        if (e instanceof UnknownHostException || e instanceof SocketTimeoutException) {
            title = "Connection";
            message = "Please check your internet connectivity";
        } else if (e instanceof JSONException || e instanceof IOException) {
            title = "Error";
            message = "There is a problem in our server and we are looking at it";
        } else {
            title = "Error";
            message = "Fatal error, our team is looking into it and will fix it for you";
        }
        showAlertDialog(title, message);
    }

    /*
        Private methods
     */

    private void drawDirectionRoute(ArrayList<LatLng> steps) {
        if (visiblePolyline != null) {
            visiblePolyline.remove();
        }
        PolylineOptions polylineOptions = new PolylineOptions().color(R.color.colorPrimaryDark);
        for (LatLng step : steps) {
            polylineOptions.add(step);
        }
        visiblePolyline = mMap.addPolyline(polylineOptions);
    }

    private void showAlertDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void buildAPIClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void setCameraOnCurrentLocation() {
        if (currentLocation != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_CAMERA_ZOOM));
        } else {
            showAlertDialog("Location", "Location is deactivated, Please activate it");
        }
    }

    private void updateCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

}
