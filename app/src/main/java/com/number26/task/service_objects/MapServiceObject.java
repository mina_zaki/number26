package com.number26.task.service_objects;

import android.content.Intent;
import android.content.IntentFilter;

import com.google.android.gms.maps.model.LatLngBounds;
import com.number26.task.activities.MapActivity;
import com.number26.task.application.ContextProvider;
import com.number26.task.models.Store;
import com.number26.task.network.GetStores;

import java.util.ArrayList;

/**
 * Created by Mina on 12/02/16.
 */
public class MapServiceObject {

    private static MapServiceObject instance;
    private MapActivity mapActivity;
    public static final String CANCEL_TASK_INTENT = "com.number26.action.task.cancel";

    private MapServiceObject() {}

    public static synchronized MapServiceObject getInstance() {
        if (instance == null) {
            instance = new MapServiceObject();
        }
        return instance;
    }

    public void fetchStores(LatLngBounds bounds, MapActivity mapActivity) {
        sendCancelBroadCastMessage();
        this.mapActivity = mapActivity;
        GetStores getStores = new GetStores(this);
        IntentFilter intentFilter = new IntentFilter(CANCEL_TASK_INTENT);
        ContextProvider.getContext().registerReceiver(getStores, intentFilter);
        getStores.executeTask(bounds);
    }

    public void getStoresRequestCallback(ArrayList<Store> stores) {
        mapActivity.fetchStoresCallback(stores);
    }

    public void getStoresRequestCallbackError(Exception e) {
        mapActivity.fetchStoresCallbackError(e);
    }

    private void sendCancelBroadCastMessage() {
        Intent intent = new Intent();
        intent.setAction(CANCEL_TASK_INTENT);
        ContextProvider.getContext().sendBroadcast(intent);
    }
}
